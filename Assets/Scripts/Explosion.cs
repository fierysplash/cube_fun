﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Specialized;
using Unity.Jobs;
using Unity.Burst;


public class Explosion : MonoBehaviour
{


    public float radius { get; set; }
    public float power { get; set; }
    private Vector3 expPoint;
    public Vector3[] Vels;
    public GameObject[] cubes;
    private bool timeIsStopped = false;
    void Start()
    {
        radius = 110;
        power = 220;
    }

    //	// Update is called once per frame

    /*[BurstCompile(CompileSynchronously = true)]
private struct MyJob : IJob
{
public float Radius;
public float Power;
private Vector3 expPoint;      
private readonly float radius;
private readonly int power;

public void Execute()
{
    if (Input.GetMouseButton(0))
    {
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
        {

            //				print (Input.mousePosition);
            //				print (Camera.main.ScreenPointToRay (Input.mousePosition));
            //				print ("this is hit: " + hit);

            if (hit.point.y < 0.1 || hit.point.y > -0.9)
            {
                expPoint = new Vector3(hit.point.x, hit.point.y + 0.4f, hit.point.z);
            }
            else
            {
                expPoint = hit.point;
            }
            Collider[] colliders = Physics.OverlapSphere(expPoint, radius);
            foreach (Collider objs in colliders)
            {
                Rigidbody rb = objs.GetComponent<Rigidbody>();

                if (rb != null)

                    rb.AddExplosionForce(-power, expPoint, radius, 3.0F);
            }
        }

    }

    if (Input.GetMouseButton(1))
    {
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
        {

            //				print (Input.mousePosition);
            //				print (Camera.main.ScreenPointToRay (Input.mousePosition));

            Collider[] colliders = Physics.OverlapSphere(hit.point, radius);
            foreach (Collider objs in colliders)
            {
                Rigidbody rb = objs.GetComponent<Rigidbody>();

                if (rb != null)
                    rb.AddExplosionForce(power, hit.point, radius, 3.0F);

            }


        }
    }

}
}*/

    void Update()
    {
        if (Input.GetKeyDown("p"))
        {
            Time.timeScale = (Time.timeScale == 1) ? 0 : 1;
        }

        if (Input.GetKeyDown("space"))
        {
            TimeStop();

        }

        if (Input.GetMouseButton(0))
        {
            LeftClick();

        }

        if (Input.GetMouseButton(1))

            RightClick();

    }
    //Pressing of space to stop time logic determines if resume or stop
    private void TimeStop()
    {

        if (!timeIsStopped)
        {
            cubes = GameObject.FindGameObjectsWithTag("cube");
            Vels = StopTime(cubes);
            timeIsStopped = true;
        }
        else
        {

            for (int i = 0; i < cubes.Length; i++)
            {
                if (cubes[i]!=null)
                {
                    cubes[i].GetComponent<Rigidbody>().isKinematic = false;
                    cubes[i].GetComponent<Rigidbody>().velocity = Vels[i];
                }
            }
            timeIsStopped = false;
        }
    }


    //action of stopping of time
    private Vector3[] StopTime(GameObject[] cubes)
    {
        Vector3[] Velocity = new Vector3[cubes.Length];
        for (int i = 0; i < cubes.Length; i++)
        {
            Velocity[i] = cubes[i].GetComponent<Rigidbody>().velocity;
            cubes[i].GetComponent<Rigidbody>().isKinematic = true;
        }

        return Velocity;
    }

    private void RightClick()
    {
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
        {

            //				print (Input.mousePosition);
            //				print (Camera.main.ScreenPointToRay (Input.mousePosition));

            Collider[] colliders = Physics.OverlapSphere(hit.point, radius);
            foreach (Collider objs in colliders)
            {
                Rigidbody rb = objs.GetComponent<Rigidbody>();

                if (rb != null)
                    rb.AddExplosionForce(power, hit.point, radius, 3.0F);

            }
        }
    }

    private void LeftClick()
    {
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
        {

            //				print (Input.mousePosition);
            //				print (Camera.main.ScreenPointToRay (Input.mousePosition));
            //				print ("this is hit: " + hit);

            if (hit.point.y < 0.1 || hit.point.y > -0.9)
            {
                expPoint = new Vector3(hit.point.x, hit.point.y + 0.4f, hit.point.z);
            }
            else
            {
                expPoint = hit.point;
            }
            Collider[] colliders = Physics.OverlapSphere(expPoint, radius);
            foreach (Collider objs in colliders)
            {
                Rigidbody rb = objs.GetComponent<Rigidbody>();
                if (rb != null)
                    rb.AddExplosionForce(-power, expPoint, radius, 3.0F);
            }
        }
    }



}
