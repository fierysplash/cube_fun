﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformAdjust : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AdjustX(string x)
    {
        transform.localScale = new Vector3(int.Parse(x), transform.localScale.y, transform.localScale.z);
    }

    public void AdjustY(string x)
    {
        transform.localScale = new Vector3(transform.localScale.x, int.Parse(x), transform.localScale.z);
    }

    public void AdjustZ(string x)
    {
        transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, int.Parse(x));
    }
}
