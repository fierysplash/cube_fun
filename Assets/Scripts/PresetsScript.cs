﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class PresetsScript : MonoBehaviour
{


    public float SpawnRate { get; set; }
    public int MaxObjects;
    public InputField totObjects;
    public Vector3 cubeScale = new Vector3(1, 1, 1);
    private float randomJunkie = 2.1f;
    // Use this for initialization

    public GameObject cube;
    // Use this for initialization


    public void ChangeTotalObjects()
    {

        MaxObjects = Int32.Parse(totObjects.text);
    }

    public void ChangeColor()
    {
        cube.GetComponent<MeshRenderer>().sharedMaterial.color = UnityEngine.Random.ColorHSV(0f, 1f, 0f, 1f, 0.5f, 0.9f, 0.5f, 1f);
    }

    public void RandomCube(bool changeit)
    {
        if (changeit)
        {
            randomJunkie += 0.01f;
            cubeScale = new Vector3(UnityEngine.Random.Range(0.3f, randomJunkie), UnityEngine.Random.Range(0.3f, randomJunkie), UnityEngine.Random.Range(0.3f, randomJunkie));
        }
        else
        {
            cubeScale = new Vector3(1, 1, 1);
            randomJunkie = 2.1f;
        }

    }
}
