﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;
public class create : MonoBehaviour
{

    // Use this for initialization
    public GameObject cube;
    public PresetsScript pres;

    private float spawnCounter;


    void FixedUpdate()
    {

        spawnCounter += Time.fixedDeltaTime;

        if (spawnCounter > pres.SpawnRate)
        {

            if (TallyScript.TotalCubes < pres.MaxObjects)
            {
                GameObject thiscube = (GameObject)Instantiate(cube, transform.position, Quaternion.identity);
                thiscube.transform.localScale = pres.cubeScale;

                spawnCounter = 0;
            }
        }
    }//EO FU


}//EO MonoBehaviour
