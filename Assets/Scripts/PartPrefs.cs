﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PartPrefs : MonoBehaviour {

	public PresetsScript ThisRoundPrefs;
	public PresetsScript nextRound;
	public Vector3 thisRoundScale;
	public Explosion expScrpt;

	public int thisRoundTot;

	public float thisRoundSpawnRate;
	public float expStr;
	public float expRad;

	public Text targetTxt;
	public Text expStrTxt;
	public Text expRadTxt;

    void Start()
    {

        ThisRoundPrefs.cubeScale = thisRoundScale;
        ThisRoundPrefs.MaxObjects = thisRoundTot;
        ThisRoundPrefs.SpawnRate = thisRoundSpawnRate;

        expScrpt.radius = expRad;
        expScrpt.power = expStr;

        targetTxt.text = "Target: " + thisRoundTot;
        expStrTxt.text = "Strength: " + expStr;
        expRadTxt.text = "Radius: " + expRadTxt;

        StartCoroutine(check());

        IEnumerator check()
        {
            if (TallyScript.TotalCubes >= thisRoundTot)
            {
                targetTxt.text = "Victory!";
                yield return new WaitForSeconds(3);
                nextRound.enabled = true;
            }

        }
    }

}
