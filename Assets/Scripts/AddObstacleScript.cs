﻿using UnityEngine;
using System.Collections;

public class AddObstacleScript : MonoBehaviour {

	public GameObject cubePref;
	public GameObject plane;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void AddObstacle(bool addorkill){

		// +40-40xz0-5y
		if (addorkill) {

			GameObject cubeAdd = (GameObject)Instantiate (cubePref, new Vector3 (Random.Range (-40f, 40f), Random.Range (-1f, 6f), Random.Range (-40f, 40f)), Quaternion.identity);
			cubeAdd.transform.localScale = new Vector3 (Random.Range (0.5f, 6f), Random.Range (0.5f, 6f), Random.Range (0.5f, 6f));

		} else {
			GameObject[] cubeDelete = GameObject.FindGameObjectsWithTag ("Obstacle");
			foreach(GameObject del in cubeDelete ){
				Destroy(del.gameObject);
			}
		}


	}

	public void ShowFloor(){

		plane.gameObject.SetActive (!plane.gameObject.activeInHierarchy);

	}
}
