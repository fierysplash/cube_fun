﻿using UnityEngine;
using System.Collections;

public class UiInteractScript : MonoBehaviour {

	private bool Guienabled = false;
	public GameObject canvas;
	// Use this for initialization
	void Start () {
		canvas.SetActive(true);
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetKeyDown ("u")) {
			canvas.SetActive(Guienabled);
			Guienabled = !Guienabled;
		}

	}
}
