﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class TallyScript : MonoBehaviour
{

    // Use this for initialization

    public PresetsScript pres;
    public static int TotalCubes;
    public Text tally;

    // Update is called once per frame
    void FixedUpdate()
    {

        if (Input.GetKey("escape"))
            Application.Quit();

        GameObject[] cubes = GameObject.FindGameObjectsWithTag("cube");
        TotalCubes = cubes.Length;
        // TotalCubes = pres.cubes.Count;	
        tally.text = "Current: " + TotalCubes;

    }
}
