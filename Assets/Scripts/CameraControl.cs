﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public Vector3 Centre = new Vector3(0, 0, 0);
    public float RotationSpeed;
    public float LockDistance;
    public float ViewAngle;
    private Matrix4x4 local_matrix;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {       
        if (Input.GetKey(KeyCode.Mouse2))//middle click
        {
            Debug.Log("middleMouse");
            transform.Translate(Vector3.left * Time.deltaTime * Input.GetAxis("Mouse X") * 5 * RotationSpeed);
            transform.Translate(Vector3.up * Time.deltaTime * -Input.GetAxis("Mouse Y") * 5 * RotationSpeed);
        }

        if (Input.GetKey("left"))
        {
            transform.Translate(Vector3.left * Time.deltaTime * RotationSpeed);
        }

        if (Input.GetKey("right"))
        {
            transform.Translate(Vector3.right * Time.deltaTime * RotationSpeed);
        }

        if (Input.GetKey("up"))
        {
            transform.Translate(Vector3.up * Time.deltaTime * RotationSpeed);
        }

        if (Input.GetKey("down"))
        {
            transform.Translate(Vector3.down * Time.deltaTime * RotationSpeed);
        }

        LockDistance += Input.mouseScrollDelta.y * -1;

        Vector3 NewPos = ((transform.position - Centre).normalized * LockDistance + Centre);

        transform.position = Vector3.Lerp(transform.position, NewPos, Time.deltaTime * RotationSpeed);

        transform.LookAt(Centre);
    }
}
