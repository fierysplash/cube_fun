﻿using UnityEngine;
using System.Collections;

public class DestroyMe : MonoBehaviour
{

    // Update is called once per frame

    MeshRenderer mat;
    void Start()
    {


    }

    void FixedUpdate()
    {


        if (transform.position.y < -300 || transform.position.y > 1000)
            Destroy(gameObject);
        if (transform.position.x < -500 || transform.position.x > 500)
            Destroy(gameObject);
        if (transform.position.z < -500 || transform.position.z > 500)
            Destroy(gameObject);

    }

    public void ChangeColor()
    {
        mat = GetComponent<MeshRenderer>();
        mat.sharedMaterial.color = Random.ColorHSV(0f, 1f, 0f, 1f, 0.5f, 0.9f, 0.5f, 1f);
    }
}
